package az.driving_school.controller;


import az.driving_school.dto.QuestionDto;
import az.driving_school.service.QuestionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/questions")
public class QuestionController {
    private final QuestionService questionService;


    @PostMapping
    public ResponseEntity<String> addQuestion(@RequestBody QuestionDto questionDto) {
        questionService.addQuestion(questionDto);
        return ResponseEntity.ok(" Question successfully added! ");
    }

    @GetMapping
    public ResponseEntity<List<QuestionDto>> getQuestions(){
        List<QuestionDto> questions = questionService.getAllQuestions();
        return ResponseEntity.ok(questions);
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> updateQuestion(@PathVariable("id")Long id, @RequestBody QuestionDto questionDto){
        questionService.updateQuestion(id, questionDto);
        return ResponseEntity.ok(" Question has been updated with new version ");
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteQuestion(@PathVariable("id") Long id){
        questionService.deleteQuestion(id);
        return ResponseEntity.ok(" Question deleted successfully! ");
    }










}
