package az.driving_school.controller;


import az.driving_school.dto.QuestionDto;
import az.driving_school.dto.SubjectDto;
import az.driving_school.service.SubjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/subjects")
public class SubjectController {
    private final SubjectService subjectService;

    @GetMapping("/{id}/questions")
    ResponseEntity<List<QuestionDto>> getQuestionsOfSubject(@PathVariable("id")Long id){
        List<QuestionDto> questions = subjectService.getQuestionsBySubjectId(id);
        return ResponseEntity.ok(questions);
    }

    @GetMapping("/{id}")
    ResponseEntity<SubjectDto> getSubject(@PathVariable("id") Long id){
        SubjectDto subjectDto = subjectService.getSubjectById(id);
        return ResponseEntity.ok(subjectDto);

    }

    @PostMapping("/")
    ResponseEntity<String> createSubject(SubjectDto subjectDto){
        subjectService.createSubject(subjectDto);
        return ResponseEntity.ok("New subject has been created successfully!");
    }

    @PutMapping("/{id}")
    ResponseEntity<String> updateSubject(@PathVariable("id") Long id, SubjectDto subjectDto){
        subjectService.editSubject(id, subjectDto);
        return ResponseEntity.ok("Subject has been updated successfully");
    }

    @DeleteMapping("/{id}")
    ResponseEntity<Boolean> deleteSubject(@PathVariable("id") Long id){
        return ResponseEntity.ok(subjectService.deleteSubject(id));
    }

    @GetMapping()
    ResponseEntity<List<SubjectDto>> getSubjects(){
        return ResponseEntity.ok(subjectService.getAllSubjects());
    }


}
