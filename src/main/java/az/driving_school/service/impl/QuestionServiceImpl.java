package az.driving_school.service.impl;

import az.driving_school.dto.QuestionDto;
import az.driving_school.entity.Question;
import az.driving_school.entity.Subject;
import az.driving_school.repository.QuestionRepo;
import az.driving_school.repository.SubjectRepo;
import az.driving_school.service.QuestionService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class QuestionServiceImpl implements QuestionService {
    private final ModelMapper modelMapper;
    private final QuestionRepo questionRepo;
    private final SubjectRepo subjectRepo;


    @Override
    public QuestionDto addQuestion(QuestionDto questionDto) {
        Question question = modelMapper.map(questionDto, Question.class);
        return modelMapper.map(questionRepo.save(question), QuestionDto.class);

    }

    @Override
    public List<QuestionDto> getAllQuestions() {
        List<Question> questions = questionRepo.findAll();
        List<QuestionDto> questionDtoList = questions
                .stream()
                .map(question -> modelMapper.map(question, QuestionDto.class))
                .collect(Collectors.toList());
        return questionDtoList;
    }

    @Override
    public Boolean deleteQuestion(Long id) {
            Optional<Question> question = questionRepo.findById(id);
            if (question.isPresent()) {
                questionRepo.deleteById(id);
                return true;
            }
            throw new RuntimeException("course with given id doesn't exist");
        }

    @Override
    public QuestionDto updateQuestion(Long id, QuestionDto questionDto) {
        Question question = questionRepo.findById(id).orElseThrow(
                () -> new RuntimeException("Question with given id not found"));

        question.setQuestionNo(questionDto.getQuestionNo());
        question.setAnswer(questionDto.getAnswer());
        question.setContent(questionDto.getContent());

        Subject subject = (Subject) subjectRepo.findById(questionDto.getSubject_id()).orElseThrow(
                () -> new RuntimeException("subject not found"));

        question.setSubject(subject);

        return modelMapper.map(questionRepo.save(question), QuestionDto.class);
    }
}
