package az.driving_school.service.impl;

import az.driving_school.dto.QuestionDto;
import az.driving_school.dto.SubjectDto;
import az.driving_school.entity.Question;
import az.driving_school.entity.Subject;
import az.driving_school.repository.SubjectRepo;
import az.driving_school.service.SubjectService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class SubjectServiceImpl implements SubjectService {
    private final SubjectRepo subjectRepo;
    private final ModelMapper modelMapper;

    @Override
    public List<QuestionDto> getQuestionsBySubjectId(Long id) {
        Subject subject = subjectRepo.findById(id).orElseThrow(
                () -> new RuntimeException("subject with given id does not exist"));

        List<Question> questions = subject.getQuestions();

        List<QuestionDto> questionDtoList = questions
                .stream()
                .map(question -> modelMapper.map(question, QuestionDto.class))
                .collect(Collectors.toList());

        return questionDtoList;


    }

    @Override
    public SubjectDto getSubjectById(Long id) {
        Optional<Subject> optSubject = subjectRepo.findById(id);
        if (optSubject.isPresent()) {
            return modelMapper.map(optSubject.get(), SubjectDto.class);
        } else throw new RuntimeException("student with given id doesn't exist");

    }

    @Override
    public List<SubjectDto> getAllSubjects() {
       return subjectRepo.findAll()
                .stream()
                .map(subject -> modelMapper.map(subject, SubjectDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public SubjectDto createSubject(SubjectDto subjectDto) {
        Subject subject = modelMapper.map(subjectDto, Subject.class);
        return modelMapper.map(subjectRepo.save(subject),SubjectDto.class);
    }

    @Override
    public SubjectDto editSubject(Long id, SubjectDto subjectDto) {
       Subject subject = subjectRepo.findById(id).orElseThrow(
               ()-> new RuntimeException("Subject not found, create a new one?"));
       subject.setName(subjectDto.getName());
       return  modelMapper.map(subjectRepo.save(subject), SubjectDto.class);
    }

    @Override
    public Boolean deleteSubject(Long id) {
           if (subjectRepo.findById(id).isPresent()){
               subjectRepo.deleteById(id);
               return  true;
           }
           return false;
    }
}