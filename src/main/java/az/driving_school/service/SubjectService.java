package az.driving_school.service;

import az.driving_school.dto.QuestionDto;
import az.driving_school.dto.SubjectDto;
import az.driving_school.entity.Subject;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public interface SubjectService {
    List<QuestionDto> getQuestionsBySubjectId(Long id);
    SubjectDto getSubjectById(Long id);
    List<SubjectDto> getAllSubjects();
    SubjectDto createSubject(SubjectDto subjectDto);
    SubjectDto editSubject(Long id, SubjectDto subjectDto);
    Boolean deleteSubject(Long id);
}
