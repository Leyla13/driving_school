package az.driving_school.service;

import az.driving_school.dto.QuestionDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface QuestionService {
    QuestionDto  addQuestion(QuestionDto questionDto);
    List<QuestionDto> getAllQuestions();
    Boolean deleteQuestion(Long id);
    QuestionDto updateQuestion(Long id, QuestionDto questionDto);
}

