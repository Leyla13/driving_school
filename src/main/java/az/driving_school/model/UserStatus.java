package az.driving_school.model;

import lombok.Getter;


@Getter
public enum UserStatus {
    PENDING(1),
    ACTIVE(2),
    LOCKED(3),
    DISABLED(4);


    private int id;

    UserStatus(int id) {
        this.id = id;
    }

}


