package az.driving_school.entity;


import az.driving_school.entity.Exam;
import az.driving_school.entity.Question;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@Table(name = "answer_history")
public class Answer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    boolean correct;

    String content;

    @ManyToOne
    @JoinColumn(name = "question_id")
    Question question;


    @ManyToOne
    @JoinColumn(name = "exam_id")
    Exam exam;

}