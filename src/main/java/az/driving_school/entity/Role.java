package az.driving_school.entity;
import lombok.Data;

import javax.persistence.*;



@Data
@Entity
@Table(name = "role")
public class Role {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    private String name;

}

