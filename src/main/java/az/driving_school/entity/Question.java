package az.driving_school.entity;


import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Entity(name = "questions")
@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class  Question {
    @Id
    Long id;

    Long questionNo;

    @Lob
    @Column(name = "question")
    String content;
    //   String photo_url;

    @Column(name = "correct_answer")
    String answer;


    @ManyToOne()
    @JoinColumn(name = "subject_id")
    Subject subject;


}

