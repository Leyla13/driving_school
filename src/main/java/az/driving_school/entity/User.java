package az.driving_school.entity;
import az.driving_school.model.UserStatus;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "username", length = 45, nullable = false, unique = true)
    String userName;

    @Column(name = "first_name", length = 45, nullable = false)
    String firstName;

    @Column(name = "first_name", length = 45, nullable = false)
    String lastName;

    @Column(length = 64, nullable = false)
    String password;

    @Column(length = 128, nullable = false, unique = true)
    String email;

    String phone;

    //  Boolean approved;
    boolean active;

    @Enumerated(EnumType.ORDINAL)
    private UserStatus status;

    //
    @CreationTimestamp
    @Column(name = "created_date",updatable = false)
    LocalDateTime createdDate;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<Role> roles = new HashSet<>();




}
