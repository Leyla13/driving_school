package az.driving_school.repository;

import az.driving_school.entity.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SubjectRepo extends JpaRepository <Subject, Long>{
    @Override
    Optional<Subject> findById(Long id);
}
