package az.driving_school.repository;

import az.driving_school.entity.Question;
import org.springframework.data.jpa.repository.JpaRepository;


public interface QuestionRepo extends JpaRepository<Question, Long> {

}