package az.driving_school.dto;

import lombok.Data;

@Data
public class QuestionDto {
    private Long questionNo;
    private String content;
    private String answer;
    private Long subject_id;
}
