package az.driving_school.dto;

public class UserDto {
    String username;
    String firstName;
    String lastName;
    String password;
    String email;
    String phone;
}
